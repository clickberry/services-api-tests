Create user(all correct tests verify tokens that they are not null):
CreateUser.jmx
1. Empty parameters (exist in request, but is empty)
	-empty email - 400
	-empty password - 400
	-empty email and password - 400
2. Mandatory (not exist in request)
	-only password - 400
	-only email - 400
	-empty request - 400
3. Whitespace (parameter contains only whitespace)
	-whitespace password - 400
	-whitespace email - 400
	-whitespace password and email - 400
4. Duplicate
	-create already existing user - 401
5. Another tokens
	-create user with another token in header - 201
6. Correct request
	-create and login - 201
	
Login user(all correct tests verify tokens that they are not null):
LoginUser.jmx
1. Empty parameters (exist in request, but is empty)
	-empty email - 400
	-empty password - 400
	-empty emiail and password - 400
2. Mandatory (not exist in request)
	-only password - 400
	-only email - 400
	-empty request - 400
3. Whitespace (parameter contains only whitespace)
	-whitespace password - 401
	-whitespace email - 401
	-whitespace password and email - 401
4. Duplicate
	-login 2 times and check that accessToken and refreshToken are unique for every request - 200
5. Not existing user - 401
6. Another tokens
	-login user with another token in header - 200
7. Correct request
	-create and login - 200
	
Refresh user:
RefreshUser.jmx
1. Not authorized. Request without header - 401
2. Empty
	-header contains authorization:"" - 401
	-header contains authorization:"JWT" - 401
3. Whitespace
	-header contains authorization:" " - 401
	-header contains authorization:"JWT  " - 401
4. Duplicate
	-header contains 2 authorization parameters with different correct tokens - 400
	-header contains 2 authorization parameters with same correct tokens - 400
5. Not existing,  correct, but not valid - 401
6. Incorrect refresh token - 401
7. Access token instead of refresh token - 401
8. Correct request and check that tokens are not equal - 200

Logout user:
LogoutUser.jmx
1. Not authorized. Request without header - 401
2. Empty
	-header contains authorization:"" - 401
	-header contains authorization:"JWT" - 401
3. Whitespace
	-header contains authorization:" " - 401
	-header contains authorization:"JWT  " - 401
4. Duplicate
	-Logout 2 times with the same refresh token - 401
5. Not existing token, correct, but not valid - 401
6. Incorrect refresh token - 401
7. Access token instead of refresh token - 401
8. Correct request and check that tokens are not equal - 200
	
Delete user:
DeleteUser.jmx
1. Not authorized. Request without header - 401
2. Empty
	-header contains authorization:"" - 401
	-header contains authorization:"JWT" - 401
3. Whitespace
	-header contains authorization:" " - 401
	-header contains authorization:"JWT  " - 401
4. Duplicate
	-delete user 1 time -200
	-delete user 2 time -401
5. Not existing,  correct, but not valid - 401
6. Incorrect access token - 401
7. Refresh token instead of access token - 401
8. Correct request and check that user can not login - 200

Get user:
GetUser.jmx
1. Not authorized. Request without header - 401
2. Empty
	-header contains authorization:"" - 401
	-header contains authorization:"JWT" - 401
3. Whitespace
	-header contains authorization:" " - 401
	-header contains authorization:"JWT  " - 401
4. Get deleted user - 401
5. Not existing,  correct, but not valid - 401
6. Incorrect access token - 401
7. Refresh token instead of access token - 401
8. Correct request and check that all user properties are correct - 200