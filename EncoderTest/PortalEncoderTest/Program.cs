﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using System.Threading;
using System.Web.Script.Serialization;
using Api;
using NLog;

namespace PortalEncoderTest
{
    class Program
    {
        private static NameValueCollection AppSettings = ConfigurationManager.AppSettings;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private static string commonResult;

        static void Main(string[] args)
        {
            if (args.Length == 0) throw new ArgumentNullException();
            if (args.Length > 1) throw new ArgumentException("Many arguments");
            var sourceFile = args[0];
            
            foreach(var file in Directory.GetFiles(AppSettings["resultFolder"], "*", SearchOption.AllDirectories))
            {
                File.Delete(file);
            }

            foreach (var dir in Directory.GetDirectories(AppSettings["resultFolder"], "*", SearchOption.AllDirectories))
            {
                Directory.Delete(dir);
            }

            List<Source> sources = new List<Source>();
            foreach(string json in (from x in File.ReadAllLines(sourceFile)  select x).ToList())
            {
                sources.Add((Source)new JavaScriptSerializer().Deserialize(json, typeof(Source)));
            }

            commonResult = Path.Combine(AppSettings["resultFolder"], "CommonResult.txt");
            Process.commonResult = commonResult;
            var projects = Process.Upload(sources);
            Validation.commonResult = commonResult;
            Validation.ValidateQualitiesAndFormatsForVideoProject(projects);
            Process.ProcessProjects(projects);
            Process.WriteCommonResult();

            _logger.Info("Test is ended. Press 'Enter' to quit.");
            Console.Read();
        }
        
    }
}
