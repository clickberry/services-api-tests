﻿using System;
using MediaInfoNET;
using NLog;

namespace Api
{
    class MediaInfo
    {
        private readonly MediaFile _mediafile;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public int Width
        {
            get
            {
                try
                {
                    return _mediafile.Video[0].Width;
                }
                catch (Exception ex)
                {
                    _logger.Error("Incorrect object in mediafile/ " + ex.Message + ex.StackTrace);
                    return -1;
                }
            }
        }
        public int Height
        {
            get
            {
                try
                {
                    return _mediafile.Video[0].Height;
                }
                catch(Exception ex)
                {
                    _logger.Error("Incorrect object in mediafile/ " + ex.Message + ex.StackTrace);
                    return -1;
                }
            }
        }

        public string ContentType
        {
            get
            {
                if (_mediafile.General.Format.ToLower().Contains("mpeg-4")) return "mp4";
                if (_mediafile.General.Format.ToLower().Contains("webm")) return "webm";
                return string.Empty;
            }
        }

        public MediaInfo(string videoPath)
        {
            _mediafile = new MediaFile(videoPath);
        }
        
    }
}
