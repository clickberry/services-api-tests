﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Web.UI.WebControls.Expressions;
using NLog;

namespace Api
{
    class Client
    {
        private static NameValueCollection AppSettings = ConfigurationManager.AppSettings;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public async Task<string> Login(string email, string password)
        {
            using (var client = new HttpClient())
            {
                _logger.Info("Try to login by user {0}", email);
                var uri = new Uri(AppSettings["authApi"] + "/signin");
                var parameters = new Dictionary<string, string>
                {
                    {"email", email},
                    {"password", password}
                };
                var content = new FormUrlEncodedContent(parameters);
                using (var response = await client.PostAsync(uri, content))
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        _logger.Info("Cannot login: RC {0}", response.StatusCode);
                        return string.Empty;
                    }
                    return await response.Content.ReadAsStringAsync();

                }
            }
        }
        public async Task<string> Register(string email, string password)
        {
            using (var client = new HttpClient())
            {
                _logger.Info("Try to register user: {0}", email);
                var uri = new Uri(AppSettings["authApi"] + "/signup");
                var parameters = new Dictionary<string, string>
                {
                    {"email", email},
                    {"password", password}
                };
                var content = new FormUrlEncodedContent(parameters);
                using (var response = await client.PostAsync(uri, content))
                {
                    if (response.StatusCode == HttpStatusCode.InternalServerError) throw new Exception("Cannot create user. RC 500");
                    if (response.StatusCode != HttpStatusCode.Created)
                    {
                        _logger.Info("User was created");
                        return string.Empty;
                    }
                    _logger.Info("User is already existed. RC {0}", response.StatusCode);
                    return await response.Content.ReadAsStringAsync();

                }
            }
        }
        public async Task<string> UploadFileAsync(string accessToken, string url, string filePath, string[] qualities, string[] formats)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
                    FileOptions.Asynchronous))
            {
                using (var content =
                        new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture))
                    )
                {
                    var streamContent = new StreamContent(fileStream);
                    streamContent.Headers.Add("Content-Type", "application/octet-stream");
                    foreach (var quality in qualities)
                    {
                        var param = new StringContent(quality);
                        content.Add(param, name: @"""Qualities""");
                    }
                    foreach (var format in formats)
                    {
                        var param = new StringContent(format);
                        content.Add(param, name: @"""Formats""");
                    }
                    content.Add(new StringContent("name"), Path.GetFileName(filePath));
                    content.Add(streamContent, "video", Path.GetFileName(filePath));

                    using (var client = new HttpClient())
                    {
                        client.Timeout = TimeSpan.FromMinutes(double.Parse(AppSettings["clientTimeoutMinutes"]));
                        _logger.Info("Timeout in {0} minutes", client.Timeout.TotalMinutes);
                        client.DefaultRequestHeaders.Add("Authorization", "JWT " + accessToken);
                        _logger.Info("POST {0}", url);
                        using (HttpResponseMessage message = await client.PostAsync(url, content))
                        {
                            if (message.StatusCode != HttpStatusCode.Created)
                            {
                                throw new ApplicationException(string.Format("Upload file {0}. Invalid response received: {1}",
                                    filePath, message.StatusCode));
                            }

                            // Read response
                            string response = await message.Content.ReadAsStringAsync();
                            //Console.WriteLine(response);
                            return response;
                        }
                    }
                }
            }
        }
        public async Task<string> UploadFileAsync(string url, string filePath, string[] qualities, string[] formats)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
                    FileOptions.Asynchronous))
            {
                using (var content =
                        new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture))
                    )
                {
                    var streamContent = new StreamContent(fileStream);
                    streamContent.Headers.Add("Content-Type", "application/octet-stream");
                    foreach(var quality in qualities)
                    {
                        var param = new StringContent(quality);
                        content.Add(param, name: @"""Qualities""");
                    }
                    foreach(var format in formats)
                    {
                        var param = new StringContent(format);
                        content.Add(param, name: @"""Formats""");
                    }                   
                    content.Add(streamContent, "video", Path.GetFileName(filePath));

                    using (var client = new HttpClient())
                    {
                        client.Timeout = TimeSpan.FromMinutes(double.Parse(AppSettings["clientTimeoutMinutes"]));
                        using (HttpResponseMessage message = await client.PostAsync(url, content))
                        {
                            if (message.StatusCode != HttpStatusCode.Created)
                            {
                                throw new ApplicationException(string.Format("Upload file {0}. Invalid response received: {1}", 
                                    filePath, message.StatusCode));
                            }

                            // Read response
                            string response = await message.Content.ReadAsStringAsync();
                            //Console.WriteLine(response);
                            return response;
                        }
                    }
                }
            }
        }
        public async Task<bool> DownloadFile(string requestUri, string filePath)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(double.Parse(AppSettings["clientTimeoutMinutes"]));
                using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
                {

                    using (var message = await client.SendAsync(request))
                    {
                        if (message.StatusCode != HttpStatusCode.OK)
                        {
                            _logger.Info("RC: " + message.StatusCode);
                            return false;
                        }
                        var contentStream = await message.Content.ReadAsStreamAsync();
                        using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                        {
                            await contentStream.CopyToAsync(fileStream);
                        }
                    }
                }
            }
            return true;
        }
    }
}
