﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class Screenshot
    {

        public string ContentType { get; set; }
        public string Uri { get; set; }

    }
}
