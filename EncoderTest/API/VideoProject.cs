﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class VideoProject
    {

        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public List<Video> Videos { get; set; }
        public Source source { get; set; }

    }
}
