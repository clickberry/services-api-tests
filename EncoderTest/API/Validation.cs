﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Api
{
    public class Validation
    {
        public static string commonResult;

        public static void ValidateQualitiesAndFormats(List<Project> projects)
        {
            var fileStream = new StreamWriter(File.Open(commonResult, FileMode.Append, FileAccess.Write));
            fileStream.WriteLine("Check of qualities and formats");

            foreach (var project in projects)
            {
                List<string> expectedQualities = new List<string>(project.source.qualities != null && project.source.qualities.Length != 0 ? project.source.qualities : project.source.expectedQualities);
                List<string> expectedFormats = new List<string>(project.source.formats != null && project.source.formats.Length != 0 ? project.source.formats : project.source.expectedFormats);
                var sourceFileInfo = new MediaInfo(project.source.Video);
                expectedQualities = (from x in expectedQualities
                                     where int.Parse(x.Substring(1)) <= sourceFileInfo.Height
                                     select x).ToList();
                if (sourceFileInfo.Height > 1080) expectedQualities.Add("S" + sourceFileInfo.Height);
                var formats = (from x in project.Videos
                               group x by x.ContentType into types
                               select types).ToArray();
                var qualities = (from x in project.Videos
                                 group x by x.Height into heights
                                 select heights.Key).ToArray();
                fileStream.WriteLine(project.source.Video);
                var error = false;
                if (expectedFormats.Count != formats.Length)
                {
                    fileStream.WriteLine("Incorrect count of formats. Expected: {0}. Actual: {1}", expectedFormats.Count, formats.Length);
                    error = true;
                }
                if (expectedQualities.Count != qualities.Length)
                {
                    fileStream.WriteLine("Incorrect count of qualities. Expected: {0}. Actual: {1}", expectedQualities.Count, qualities.Length);
                    error = true;
                }
                var parsedExpectedQualities = (from x in expectedQualities
                                               select int.Parse(x.Substring(1))).ToList();
                var qualitiesList = new List<int>(qualities);
                var diff1 = parsedExpectedQualities.Except(qualitiesList).ToList();
                var diff2 = qualitiesList.Except(parsedExpectedQualities).ToList();
                if (diff1.Count > 0)
                {
                    fileStream.WriteLine("Incorrect expected qualities: {0}", string.Join(", ", diff1));
                    error = true;
                }
                if (diff2.Count > 0)
                {
                    fileStream.WriteLine("Incorrect actual qualities: {0}", string.Join(", ", diff2));
                    error = true;
                }
                if (!error) fileStream.WriteLine("OK");
                fileStream.WriteLine();
            }
            fileStream.WriteLine();
            fileStream.Dispose();
        }
        public static void ValidateQualitiesAndFormatsForVideoProject(List<Project> projects)
        {
            var fileStream = new StreamWriter(File.Open(commonResult, FileMode.Append, FileAccess.Write));
            fileStream.WriteLine("Check of qualities and formats");

            foreach (var project in projects)
            {
                var expectedQualities = new List<string>(project.source.qualities != null && project.source.qualities.Length != 0 ? project.source.qualities : project.source.expectedQualities);
                var expectedFormats = new List<string> {"mp4", "webm"};//new List<string>(project.source.formats != null && project.source.formats.Length != 0 ? project.source.formats : project.source.expectedFormats);
                var sourceFileInfo = new MediaInfo(project.source.Video);

                expectedQualities = (from x in expectedQualities
                                     where x.StartsWith("S") && int.Parse(x.Substring(1)) <= sourceFileInfo.Height
                                     select x).ToList();
                if (sourceFileInfo.Height > 1080) expectedQualities.Add("S" + sourceFileInfo.Height);

                var formats = (from x in project.Videos
                               group x by x.ContentType into types
                               select types).ToArray();

                var qualities = (from x in project.Videos
                                 group x by x.Height into heights
                                 select heights.Key).ToArray();

                fileStream.WriteLine(project.source.Video);
                var error = false;
                if (expectedFormats.Count != formats.Length)
                {
                    fileStream.WriteLine("Incorrect count of formats. Expected: {0} ({1}). Actual: {2}", expectedFormats.Count, string.Join(", ", expectedFormats),
                                                                                                                formats.Length);
                    error = true;
                }
                if (expectedQualities.Count != qualities.Length)
                {
                    fileStream.WriteLine("Incorrect count of qualities. Expected: {0} ({1}). Actual: {2} ({3})", expectedQualities.Count, string.Join(", ", expectedQualities),
                                                                                                                qualities.Length, string.Join(", ", qualities));
                    error = true;
                }
                var parsedExpectedQualities = (from x in expectedQualities
                                               select int.Parse(x.Substring(1))).ToList();
                var qualitiesList = new List<int>(qualities);
                var diff1 = parsedExpectedQualities.Except(qualitiesList).ToList();
                var diff2 = qualitiesList.Except(parsedExpectedQualities).ToList();
                if (diff1.Count > 0)
                {
                    fileStream.WriteLine("Incorrect expected qualities: {0}", string.Join(", ", diff1));
                    error = true;
                }
                if (diff2.Count > 0)
                {
                    fileStream.WriteLine("Incorrect actual qualities: {0}", string.Join(", ", diff2));
                    error = true;
                }
                fileStream.WriteLine("PostedUrl: {0}", project.PostedUrl);
                if (!error)
                {
                    fileStream.WriteLine("Qualities: {0}", string.Join(", ", expectedQualities));
                    fileStream.WriteLine("OK");
                }
                fileStream.WriteLine();
            }
            fileStream.WriteLine();
            fileStream.Dispose();
        } 
    
    }

}