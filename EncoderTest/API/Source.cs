﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class Source
    {

        public string Video { get; set; }
        public string[] formats { get; set; }
        public string[] qualities { get; set; }
        public string[] expectedFormats { get; set; }
        public string[] expectedQualities { get; set; }

    }
}
