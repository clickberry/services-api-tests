﻿
namespace Api
{
    public class EncoderVideo
    {

        public string ContentType { get; set; }
        public string Uri { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

    }
}
