﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using NLog;

namespace Api
{
    public class Process
    {
        private static NameValueCollection AppSettings = ConfigurationManager.AppSettings;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public static string commonResult;

        public static void ProcessProjects(List<Project> projects)
        {
            var client = new Client();

            var maxWait = int.Parse(AppSettings["waitingCycle"]);
            var currentCycle = 0;
            while (projects.Count != 0 || currentCycle < maxWait)
            {
                currentCycle++;
                for (var i = 0; i < projects.Count; i++)
                {
                    for (var j = 0; j < projects[i].Videos.Count; j++)
                    {

                        var ouputVideo = projects[i].Videos[j];
                        var type = GetType(ouputVideo.ContentType);
                        var fileFolder = Path.Combine(AppSettings["resultFolder"], Path.GetFileName(projects[i].source.Video));
                        if (!Directory.Exists(fileFolder)) Directory.CreateDirectory(fileFolder);
                        var filePath = Path.Combine(fileFolder, ouputVideo.Width + "x" + ouputVideo.Height + "." + type);
                        _logger.Info("Try to download {0}", ouputVideo.Uri);
                        var downloaded = client.DownloadFile(ouputVideo.Uri, filePath).Result;
                        if (downloaded)
                        {
                            using (var fileStream = new StreamWriter(File.Open(Path.Combine(fileFolder, "Result.txt"), FileMode.Append, FileAccess.Write)))
                            {
                                _logger.Info("File is encoded");
                                fileStream.WriteLine(Path.GetFileName(filePath));
                                var videoFile = new MediaInfo(filePath);
                                fileStream.WriteLine("<begin>");
                                var error = false;
                                if (!type.ToLower().Equals(videoFile.ContentType.ToLower()))
                                {
                                    error = true;
                                    _logger.Info("Incorrect type");
                                    fileStream.WriteLine("Incorrect type");
                                }
                                if (ouputVideo.Width != videoFile.Width)
                                {
                                    error = true;
                                    _logger.Info("Incorrect width");
                                    fileStream.WriteLine("Incorrect width");
                                }
                                if (ouputVideo.Height != videoFile.Height)
                                {
                                    error = true;
                                    _logger.Info("Incorrect height");
                                    fileStream.WriteLine("Incorrect height");
                                }
                                if (!error)
                                {
                                    _logger.Info("All parameters are OK");
                                    fileStream.WriteLine("All parameters are OK");
                                }
                                fileStream.WriteLine("<end>\n");
                            }
                            projects[i].Videos.Remove(ouputVideo);
                            j--;
                            continue;
                        }
                        _logger.Info("File is not encoded yet. Pause for 5 sec");
                        Thread.Sleep(5000);
                    }

                    if (projects[i].Videos.Count != 0) continue;
                    projects.Remove(projects[i]);
                    i--;
                }
            }
        }

        public static string GetType(string contentType)
        {
            var index = contentType.LastIndexOf('/');
            if (index == -1) return contentType;
            return contentType.Substring(index + 1);
        }

        public static void WriteCommonResult()
        {
            var resultFiles = Directory.GetFiles(AppSettings["resultFolder"], "Result.txt", SearchOption.AllDirectories);

            //if (File.Exists(commonResult)) File.Delete(commonResult);
            using (var fileStream = new StreamWriter(File.Open(commonResult, FileMode.Append, FileAccess.Write)))
            {
                foreach (var file in resultFiles)
                {
                    fileStream.WriteLine(Path.GetDirectoryName(file));
                    foreach (var line in File.ReadAllLines(file))
                    {
                        fileStream.WriteLine(line);
                    }
                    fileStream.WriteLine();
                    fileStream.WriteLine();
                }
            }
        }
        public static List<Project> Upload(List<Source> sources)
        {
            var client = new Client();
            var uploadAttempts = int.Parse(AppSettings["uploadAttempts"]);
            var projects = new List<Project>();
            var userIndex = -1;
            if (string.IsNullOrWhiteSpace(AppSettings["email"])) throw new ArgumentException("Email parameter in config is empty");
            var users = AppSettings["email"].Split(new[] { ";" }, StringSplitOptions.None);
            _logger.Info(string.Join(", ", users));
            var formats = new[] { "mp4", "webm" };//source.formats == null || source.formats.Length == 0 ? "<empty>" : string.Join(", ", source.formats);
            
            foreach (var source in sources)
            {
                var qualities = source.qualities == null || source.qualities.Length == 0 ? "<empty>" : string.Join(", ", source.qualities);
                _logger.Info("Upload file {0} with formats: {1}, qualities: {2}", source.Video, string.Join(", ", formats), qualities);
                var failed = true;
                
                userIndex++;
                if (userIndex >= users.Length) userIndex = 0;
                var userEmail = users[userIndex];
                
                for (var i = 0; i < uploadAttempts; i++)
                {
                    
                    try
                    {
                        var parameters = source.qualities == null? string.Empty : string.Join(",", source.qualities);
                        _logger.Info("Attempt {0} of {1} to upload video file", i + 1, uploadAttempts);

                        var result = client.Register(userEmail, AppSettings["password"]).Result;
                        if (result.Equals(string.Empty)) result = client.Login(userEmail, AppSettings["password"]).Result;
                        var tokens = (Token) new JavaScriptSerializer().Deserialize(result, typeof (Token));
                        var accessToken = tokens.AccessToken;
                        var videoApi = AppSettings["videoApi"];
                        if (!string.IsNullOrEmpty(parameters)) videoApi += "/?quality=" + parameters;
                        var resp = client.UploadFileAsync(accessToken, videoApi, source.Video, source.qualities, source.formats).Result;
                        var res = (Project)new JavaScriptSerializer().Deserialize(resp, typeof(Project));
                        res.source = source;
                        res.PostedUrl = videoApi;
                        projects.Add(res);
                        _logger.Info("File {0} urls {1}", source.Video, string.Join(", ", res.Videos.Select(x => x.Uri)));
                        failed = false;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        continue;
                    }
                    break;
                }
                if (failed)
                {
                    _logger.Info("It's impossible to upload video {0}", source.Video);
                    using (var fileStream = new StreamWriter(File.Open(commonResult, FileMode.Append, FileAccess.Write)))
                    {
                        fileStream.WriteLine("It's impossible to upload video {0}", source.Video);
                        fileStream.WriteLine();
                    }

                }
            }
            return projects;
        } 
        public static List<Project> GetProjects(List<Source> sources)
        {
            var client = new Client();
            var portalUrl = AppSettings["portalUrl"];
            var uploadAttempts = int.Parse(AppSettings["uploadAttempts"]);
            var projects = new List<Project>();
            foreach (var source in sources)
            {
                var formats = source.formats == null || source.formats.Length == 0 ? "<empty>" : string.Join(", ", source.formats);
                var qualities = source.qualities == null || source.qualities.Length == 0 ? "<empty>" : string.Join(", ", source.qualities);
                _logger.Info("Upload file {0} with formats: {1}, qualities: {2}", source.Video, formats, qualities);
                bool Failed = true;
                for (var i = 0; i < uploadAttempts; i++)
                {
                    try
                    {
                        _logger.Info("Attempt {0} of {1} to upload video file", i + 1, uploadAttempts);
                        var resp = client.UploadFileAsync(portalUrl + "/api/videos", source.Video, source.qualities, source.formats).Result;
                        var res = (Project)new JavaScriptSerializer().Deserialize(resp, typeof(Project));
                        res.source = source;
                        projects.Add(res);
                        using (var logFile = new StreamWriter("log.txt", true))
                        {
                            logFile.WriteLine("File {0} urls {1}", source.Video, string.Join(", ", res.Videos.Select(x => x.Uri)));
                        }
                        Failed = false;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        using (var logFile = new StreamWriter("log.txt", true))
                        {
                            logFile.WriteLine("File {0} with exception {1} {2}", source.Video, ex.Message, ex.StackTrace);
                        }
                        continue;
                    }
                    break;
                }
                if (Failed)
                {
                    _logger.Info("Sorry, kick off file");
                    using (var fileStream = new StreamWriter(File.Open(commonResult, FileMode.Append, FileAccess.Write)))
                    {
                        fileStream.WriteLine("It's impossible to upload video {0}", source.Video);
                        fileStream.WriteLine();
                    }

                }
            }
            return projects;
        } 
    }
}