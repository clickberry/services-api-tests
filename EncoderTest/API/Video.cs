﻿
namespace Api
{
    public class Video
    {

        public string ContentType { get; set; }
        public string Uri { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Sign { get; set; }

    }
}
