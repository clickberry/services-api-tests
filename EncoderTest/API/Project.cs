﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api
{
    public class Project
    {

        public string Id { get; set; }
        public string Uri { get; set; }
        public List<EncoderVideo> Videos { get; set; }
        public List<Screenshot> Screenshots { get; set; }
        public Source source { get; set; }

        public string PostedUrl { get; set; }

    }
}
