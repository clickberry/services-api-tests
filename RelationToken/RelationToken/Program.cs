﻿using System;
using System.Collections.Generic;

namespace RelationToken
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                if (args[0].Equals("-h") || args[0].Equals("/?") || args[0].Equals("--help"))
                {
                    Help();
                    return;
                }
            }

            if (args.Length < 2)
            {
                Help();
                throw new ArgumentException();
            }
            
            var secretKey = args[0];
            var payload = new Dictionary<string, object>();
            for (var i = 1; i < args.Length; i++)
            {
                var value = args[i];
                var colonIndex = value.IndexOf(':');
                if (colonIndex == -1)
                {
                    Help();
                    throw new ArgumentException("No ':' in payload: " + value);
                }
                var name = value.Substring(0, colonIndex);
                var val = value.Substring(colonIndex + 1);
                int intObj;
                float floatObj;
                object obj = val;
                if (int.TryParse(val, out intObj))obj = intObj;
                else if (float.TryParse(val, out floatObj)) obj = floatObj;
                payload.Add(name, obj);
            }

            Console.WriteLine(JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256));
        }

        static void Help()
        {
            Console.WriteLine("Use: RelationToken.exe ${token} id:${id} ownerId:${ownerId} ...");
        }
    }
}
