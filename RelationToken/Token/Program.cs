﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelationToken
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0 || args.Length > 3)
            {
                Help();
                throw new ArgumentException();
            }
            if (args.Length == 1)
            {
                if (args[0].Equals("-h") || args[0].Equals("/?") || args[0].Equals("--help"))
                {
                    Help();
                    return;
                }
            }
            var payload = new Dictionary<string, object>()
            {
                { "id", args[0] },
                { "ownerId", args[1] }
            };
            var secretKey = args[2];
            Console.WriteLine(JWT.JsonWebToken.Encode(payload, secretKey, JWT.JwtHashAlgorithm.HS256));
        }

        static void Help()
        {
            Console.WriteLine("Use: RealtionToken.exe ${id} ${ownerId} ${secretToken}");
        }
    }
}
